class Temperature
  attr_reader :c, :f
  def initialize(hash)
    @f = hash[:f]
    @c = hash[:c]
  end

  def self.from_celsius(num)
    self.new(:c => num)
  end

  def self.from_fahrenheit(num)
    self.new(:f => num)
  end

  def in_fahrenheit()
    if self.f == nil
    (self.c.to_f * 1.8) + 32
    else
    self.f
    end
  end

  def in_celsius()
    if self.c == nil
     (self.f - 32) * (5.0 / 9.0)
    else
     self.c
    end
  end
end

class Celsius < Temperature
  attr_reader :c, :f

  def initialize(num)
    @c = num
  end

end

class Fahrenheit < Temperature
  attr_reader :c, :f

  def initialize(num)
    @f = num
  end

end
