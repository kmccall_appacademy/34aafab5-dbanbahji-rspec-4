class Dictionary
  def initialize(hash = {})
    @dictionary = hash
  end

  def entries()
    @dictionary
  end

  def add(input)
    if input.is_a?(String)
      @dictionary.merge!(input => nil)
    else
      @dictionary.merge!(input)
    end
  end

  def keywords()
    @dictionary.keys.sort
  end

  def include?(key)
    @dictionary.keys.include?(key)
  end

  def find(prefix)
    @dictionary.select{|key, val| key.start_with?(prefix)}
  end

  def printable
    printable_arr = []
    @dictionary.sort.each do |key, val|
      printable_arr << "[#{key}] \"#{val}\""
    end
    printable_arr.join("\n")
  end
end
